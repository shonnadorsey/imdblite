# == Schema Information
#
# Table name: movies
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  rating     :string(255)
#  length     :string(255)
#  created_at :datetime
#  updated_at :datetime
#  spoiler    :string(255)
#  rotten_id  :integer
#  imdb_id    :integer
#  year       :integer
#  plot       :text
#

class Movie < ActiveRecord::Base
  paginates_per 10
  
  has_many :movie_genres
  has_many :genres, through: :movie_genres
end
