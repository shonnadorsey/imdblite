# == Schema Information
#
# Table name: movie_genres
#
#  id         :integer          not null, primary key
#  movie_id   :integer
#  genre_id   :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_movie_genres_on_genre_id  (genre_id)
#  index_movie_genres_on_movie_id  (movie_id)
#

class MovieGenre < ActiveRecord::Base
  belongs_to :movie
  belongs_to :genre
end
