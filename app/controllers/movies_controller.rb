class MoviesController < ApplicationController
  def index
    @movies = Movie.order(:title).page(params[:page])
  end

  def show
    @movie = Movie.find(params[:id])
  end
  
  def new
    @movie = Movie.new
  end
  
  def create
    @movie = Movie.new(movie_params)
    @movie.save
    redirect_to '/movies'
  end
  
  private
  def movie_params
    
    params.require(:movie).permit(:title, :rating, :plot)
    end
end
