class AddSpoilerToMovies < ActiveRecord::Migration
  def change
    add_column :movies, :spoiler, :string
  end
end
