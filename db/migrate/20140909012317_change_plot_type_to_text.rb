class ChangePlotTypeToText < ActiveRecord::Migration
  def change
        remove_column :movies, :plot
    add_column :movies, :plot, :text
  end
end
