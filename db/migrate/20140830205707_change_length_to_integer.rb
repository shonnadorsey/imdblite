class ChangeLengthToInteger < ActiveRecord::Migration
  def change
    remove_column :movies, :length
    add_column :movies, :length, :integer
  end
end
