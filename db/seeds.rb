

Movie.create([
  {id: 1, title: '300', rating: 'R', length: 117, year: 2006, plot: 'King Leonidas and a force of 300 men fight the Persians at Thermopylae in 480 B.C.'},
  {id: 2, title: 'Rocky', rating: 'PG', length: 119, year: 1976, plot: 'Rocky Balboa, a small-time boxer gets a supremely rare chance to fight the heavy-weight champion, Apollo Creed, in a bout in which he strives to go the distance for his self-respect.'},
  {id: 3, title: 'Blade Runner', rating: 'R', length: 117, year: 1982, plot: 'A blade runner must pursue and try to terminate four replicants who stole a ship in space and have returned to Earth to find their creator.'},
  {id: 4, title: 'Predator', rating: 'R', length: 107, year: 1987, plot: 'A team of commandos, on a mission in a Central American jungle, find themselves hunted by an extra-terrestrial warrior.'},
  {id: 5, title: 'Night of the Living Dead', rating: 'R', year: 1968, length: 97, plot: 'A group of people hide from bloodthirsty zombies in a farmhouse.'},
  {id: 6, title: 'The Wizard of Oz', rating: 'PG', year: 1939, length: 102, plot: 'Dorothy Gale is swept away to a magical land in a tornado and embarks on a quest to see the Wizard who can help her return home.'},
  {id: 7, title: 'The Sixth Sense', rating: 'PG-13', year: 1999, length: 105, plot: 'A boy who communicates with spirits that dont know they are dead seeks the help of a disheartened child psychologist.'},
  {id: 8, title: 'Psycho', rating: 'R', year: 1960, length: 109, plot: 'A Phoenix secretary steals $40,000 from her employers client, goes on the run and checks into a remote motel run by a young man under the domination of his mother.'},
  {id: 9, title: 'Soylent Green', rating: 'PG', year: 1973, length: 97, plot: 'With the world ravaged by the greenhouse effect and overpopulation, an NYPD detective investigates the murder of a CEO with ties to the worlds main food supply.'},
  {id: 10, title: 'Planet of the Apes', rating: 'G', year: 1968, length: 112, plot: 'An astronaut crew crash lands on a planet in the distant future where intelligent talking apes are the dominant species, and humans are the oppressed and enslaved.'},
  {id: 11, title: 'The Usual Suspects', rating: 'R', year: 1995, length: 106, plot: 'A sole survivor tells of the twisty events leading up to a horrific gun battle on a boat, which begin when five criminals meet at a seemingly random police lineup.'},
  {id: 12, title: 'Fight Club', rating: 'R', year: 1999, length: 139, plot: 'An insomniac office worker looking for a way to change his life crosses paths with a devil-may-care soap maker and they form an underground fight club that evolves into something much, much more.'},
  {id: 13, title: 'Star Wars: Episode V - The Empire Strikes Back', rating: 'PG', year: 1980, length: 124, plot: 'After the rebels have been brutally overpowered by the Empire on their newly established base, Luke Skywalker takes advanced Jedi training with Master Yoda, while his friends are pursued by Darth Vader as part of his plan to capture Luke.'},
  {id: 14, title: 'Friday the 13th', rating: 'R', year: 1980, length: 95, plot: 'Camp counselors are stalked and murdered by an unknown assailant while trying to reopen a summer camp that was the site of a childs drowning.'},
  {id: 15, title: 'Oldboy', rating: 'R', year: 2003, length: 120, plot: 'After being kidnapped and imprisoned for 15 years, Oh Dae-Su is released, only to find that he must find his captor in 5 days.'},
  {id: 16, title: 'Monty Python and the Holy Grail', rating: 'PG', year: 1975, length: 91, plot: 'King Arthur and his knights embark on a low-budget search for the Grail, encountering many very silly obstacles.'},
  {id: 17, title: 'Evil Dead II', rating: 'R', year: 1987, length: 84, plot: 'The lone survivor of an onslaught of flesh-possessing spirits holds up in a cabin with a group of strangers while the demons continue their attack.'},
  {id: 18, title: 'The Godfather', rating: 'R', year: 1972, length: 175, plot: 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.'},
  {id: 19, title: 'North by Northwest', rating: 'NR', year: 1959, length: 136, plot: 'A hapless New York advertising executive is mistaken for a government agent by a group of foreign spies, and is pursued across the country while he looks for a way to survive.'},
  {id: 20, title: 'Toy Story 2', rating: 'G', year: 1999, length: 92, plot: 'When Woody is stolen by a toy collector, Buzz and his friends vow to rescue him, but Woody finds the idea of immortality in a museum tempting.'},
  {id: 21, title: 'Rashomon', rating: 'NR', year: 1950, length: 88, plot: 'A heinous crime and its aftermath are recalled from differing points of view.'},
  {id: 22, title: 'Up', rating: 'PG', year: 2009, length: 96, plot: 'By tying thousands of balloons to his home, 78-year-old Carl sets out to fulfill his lifelong dream to see the wilds of South America. Russell, a wilderness explorer 70 years younger, inadvertently becomes a stowaway.'},
  {id: 23, title: 'From Russia with Love', rating: 'PG', year: 1963, length: 115, plot: 'James Bond willingly falls into an assassination ploy involving a naive Russian beauty in order to retrieve a Soviet encryption device that was stolen by SPECTRE.'},
  {id: 24, title: 'Strangers on a Train', rating: 'PG', year: 1951, length: 101, plot: 'A psychotic socialite confronts a pro tennis star with a theory on how two complete strangers can get away with murder...a theory that he plans to implement.'},
  {id: 25, title: 'Aliens', rating: 'R', year: 1986, length: 137, plot: 'The planet from Alien (1979) has been colonized, but contact is lost. This time, the rescue team has impressive firepower, but will it be enough?'},
  {id: 26, title: 'The Good, the Bad and the Ugly', rating: 'R', year: 1966, length: 161, plot: 'A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery.'},
  {id: 27, title: 'Star Wars: Episode IV - A New Hope', rating: 'PG', year: 1977, length: 121, plot: 'Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a wookiee and two droids to save the universe from the Empire\'s world-destroying battle-station, while also attempting to rescue Princess Leia from the evil Darth Vader.'},
  {id: 28, title: 'Star Wars: Episode VI - Return of the Jedi', rating: 'PG', year: 1983, length: 134, plot: 'After rescuing Han Solo from the palace of Jabba the Hutt, the rebels attempt to destroy the second Death Star, while Luke struggles to make Vader shake off of the dark side of the Force.'},
  {id: 29, title: 'Beauty and the Beast', rating: 'G', year: 1991, length: 84, plot: 'Belle, whose father is imprisoned by the Beast, offers herself instead and discovers her captor to be an enchanted prince.'},
  {id: 30, title: 'Saving Private Ryan', rating: 'R', year: 1998, length: 169, plot: 'Following the Normandy Landings, a group of U.S. soldiers go behind enemy lines to retrieve a paratrooper whose brothers have been killed in action.'},
  {id: 31, title: 'Casablanca', rating: 'PG', year: 1942, length: 102, plot: 'Set in unoccupied Africa during the early days of World War II: An American expatriate meets a former lover, with unforeseen complications.'},
  {id: 32, title: 'The Princess Bride', rating: 'PG', year: 1987, length: 98, plot: 'A classic fairy tale, with swordplay, giants, an evil prince, a beautiful princess, and yes, some kissing (as read by a kindly grandfather).'},
  {id: 33, title: 'Gladiator', rating: 'R', year: 2000, length: 155, plot: 'When a Roman general is betrayed and his family murdered by an emperor\'s corrupt son, he comes to Rome as a gladiator to seek revenge.'},
  {id: 34, title: 'Shaun of the Dead', rating: 'R', year: 2004, length: 99, plot: 'A man decides to turn his moribund life around by winning back his ex-girlfriend, reconciling his relationship with his mother, and dealing with an entire community that has returned from the dead to eat the living.'},
  {id: 35, title: 'Mad Max', rating: 'R', year: 1979, length: 88, plot: 'A vengeful Australian policeman sets out to avenge his partner, his wife and his son whom were murdered by a motorcycle gang in retaliation for the death of their leader.'},
  {id: 36, title: 'Raiders of the Lost Ark', rating: 'PG', year: 1981, length: 115, plot: 'Archeologist and adventurer Indiana Jones is hired by the US government to find the Ark of the Covenant before the Nazis.'},
  {id: 37, title: 'The Lord of the Rings: The Fellowship of the Ring', rating: 'PG-13', year: 2001, length: 178, plot: 'A meek hobbit of the Shire and eight companions set out on a journey to Mount Doom to destroy the One Ring and the dark lord Sauron.'},
  {id: 38, title: 'The Lord of the Rings: The Two Towers', rating: 'PG-13', year: 2002, length: 179, plot: 'While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Saurons new ally, Saruman, and his hordes of Isengard.'},
  {id: 39, title: 'The Lord of the Rings: The Return of the King ', rating: 'PG-13', year: 2003, length: 201, plot: 'Gandalf and Aragorn lead the World of Men against Saurons army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.'},
  {id: 40, title: 'The Sting', rating: 'PG', year: 1973, length: 129, plot: 'In 1930s Chicago, a young con man seeking revenge for his murdered partner teams up with a master of the big con to win a fortune from a criminal banker.'},
  {id: 41, title: 'The Expendables', rating: 'R', year: 2010, length: 103, plot: 'A CIA operative hires a team of mercenaries to eliminate a Latin dictator and a renegade CIA agent.'}
])

# Update imdb_id & RottenTomato IDs
# http://developer.rottentomatoes.com/io-docs
# ——————————————————————————————————
Movie.update(1, {imdb_id: '0416449', rotten_id: 344023668})
Movie.update(2, {imdb_id: '0075148', rotten_id: 11405})
Movie.update(3, {imdb_id: '0083658', rotten_id: 12886})
Movie.update(4, {imdb_id: '0093773', rotten_id: 16751})
Movie.update(5, {imdb_id: '0063350', rotten_id: 50375489})
Movie.update(6, {imdb_id: '0032138', rotten_id: 9867})
Movie.update(7, {imdb_id: '0167404', rotten_id: 10054})
Movie.update(8, {imdb_id: '0054215', rotten_id: 12879})
Movie.update(9, {imdb_id: '0070723', rotten_id: 10181})
Movie.update(10, {imdb_id: '0063442', rotten_id: 11517})
Movie.update(11, {imdb_id: '0114814', rotten_id: 16303})
Movie.update(12, {imdb_id: '0137523', rotten_id: 13153})
Movie.update(13, {imdb_id: '0080684', rotten_id: 11470})
Movie.update(14, {imdb_id: '0080761', rotten_id: 16819})
Movie.update(15, {imdb_id: '0364569', rotten_id: 22533})
Movie.update(16, {imdb_id: '0071853', rotten_id: 11450})
Movie.update(17, {imdb_id: '0092991', rotten_id: 98831005})
Movie.update(18, {imdb_id: '0068646', rotten_id: 12911})
Movie.update(19, {imdb_id: '0053125', rotten_id: 18639})
Movie.update(20, {imdb_id: '0120363', rotten_id: 9414})
Movie.update(21, {imdb_id: '0042876', rotten_id: 17052})
Movie.update(22, {imdb_id: '1049413', rotten_id: 770671912})
Movie.update(23, {imdb_id: '0057076', rotten_id: 10737})
Movie.update(24, {imdb_id: '0044079', rotten_id: 10699})
Movie.update(25, {imdb_id: '0090605', rotten_id: 13240})
Movie.update(26, {imdb_id: '0060196', rotten_id: 13308})
Movie.update(27, {imdb_id: '0076759', rotten_id: 11292})
Movie.update(28, {imdb_id: '0086190', rotten_id: 11366})
Movie.update(29, {imdb_id: '0101414', rotten_id: 9980})
Movie.update(30, {imdb_id: '0120815', rotten_id: 13217})
Movie.update(31, {imdb_id: '0034583', rotten_id: 10056})
Movie.update(32, {imdb_id: '0093779', rotten_id: 12686})
Movie.update(33, {imdb_id: '0172495', rotten_id: 13065})
Movie.update(34, {imdb_id: '0365748', rotten_id: 12873})
Movie.update(35, {imdb_id: '0079501', rotten_id: 16235})
Movie.update(36, {imdb_id: '0082971', rotten_id: 23529})
Movie.update(37, {imdb_id: '0120737', rotten_id: 12769})
Movie.update(38, {imdb_id: '0167261', rotten_id: 10513})
Movie.update(39, {imdb_id: '0167260', rotten_id: 10156})
Movie.update(40, {imdb_id: '0070735', rotten_id: 22491})
Movie.update(41, {imdb_id: '1320253', rotten_id: 770802244})

