# == Schema Information
#
# Table name: movies
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  rating     :string(255)
#  length     :string(255)
#  created_at :datetime
#  updated_at :datetime
#  spoiler    :string(255)
#  rotten_id  :integer
#  imdb_id    :integer
#  year       :integer
#  plot       :text
#

require 'rails_helper'

RSpec.describe Movie, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
