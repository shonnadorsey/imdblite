require 'rails_helper'

RSpec.describe WelcomeController, :type => :controller do

  describe "GET index" do
    it "returns http success" do
      get :index
      expect(response).to be_success
    end
  end

  describe "GET abouts" do
    it "returns http success" do
      get :abouts
      expect(response).to be_success
    end
  end

end
